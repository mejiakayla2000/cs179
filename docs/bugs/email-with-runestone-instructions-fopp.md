If you could enroll in this version of the FOPP course in Runestone it will make it easier for me to help you in your learning and follow your progress:
https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/index.html
You may need to paste this text into the "course name" field in runestone during enrollment:
sdccd_mesa_college_cs179_spring23
